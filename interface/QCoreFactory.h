#ifndef QCOREFACTORY_H
#define QCOREFACTORY_H

#include <interface/QCore.h>
#include <interface/PixelModule.h>
#include <stdexcept>


class QCoreFactory{
    public:
        QCoreFactory(int col_factor=4, int row_factor=4);
        
        vector<vector<QCore>> loop_module_pixels(PixelModule & pm, int nrows_perchip, int ncols_perchip, vector<int> top_edges = vector<int>{0,336,2*336,3*336}, vector<int> left_edges = vector<int>{0});
        vector<QCore> from_pixel_module(PixelModule & pm);
        vector<vector<QCore>> from_pixel_module_perchip(PixelModule & pm);

    private:
        int col_factor;
        int row_factor;
};
#endif //  QCOREFACTORY_H
