#include <interface/QCore.h>
#include<interface/Util.h>
QCore::QCore(
             uint32_t ccol_in,
             uint32_t qcrow_in,
             bool isneighbour_in,
             bool islast_in,
             vector<uint32_t> adcs_in
             ) :
    adcs(adcs_in),
    isneighbour(isneighbour_in),
    islast(islast_in),
    qcrow(qcrow_in),
    ccol(ccol_in)
    {
    };



vector<bool> QCore::get_hitmap() {
    assert(adcs.size()==16);

    std::vector<bool> hitmap;

    hitmap.reserve(16);
    for(auto const adc : this->adcs){
        hitmap.push_back(adc>0);
    }
    assert(hitmap.size()==16);

    return hitmap;
};

vector<bool> QCore::get_row(int row_index) const {
    if((row_index < 0) or (row_index > 1)){
        throw;
    }
    vector<bool> row;
    row.insert(row.end(), this->hitmap.begin() + 8 * row_index, this->hitmap.begin() + 8 * (row_index+1));
    assert(row.size() == 8);
    return row;
}