#include <assert.h>
#include <interface/AuroraFormatter.h>
#include <interface/Util.h>
#include <iostream>
vector<bool> AuroraFormatter::apply_blocking(vector<bool> & stream, int chip_id) {
    vector<bool> new_stream;
    int stream_size = stream.size();
    new_stream.reserve(stream_size + (stream_size%60) * 3);

    // Allowed values for chip ID are:
    // -1         -> Means no chip ID will be written to stream (single chip mode)
    // 0, 1, 2, 3 -> Valid chip IDs for multi-chip mode
    assert((chip_id<0) | (chip_id<4));
    vector<bool> binary_chip_id = int_to_binary(chip_id,2);
    assert(binary_chip_id.size()==2);

    // AURORA blocks are 64 bits
    // step size is the amount of stream data we can pack
    // into one block (64 -  1 new stream bit - 2 chip ID bits if applicable)
    int step = chip_id > 0 ? (64 - 1 - 2) : (64 - 1);

    int index = 0;
    int bits_written = 0;
    while(index <= ceil(stream_size / step)) {
        // New stream bit
        new_stream.push_back(new_stream.size()==0);

        // Chip ID
        if(chip_id > 0) {
            new_stream.insert(
                                new_stream.end(),
                                binary_chip_id.begin(),
                                binary_chip_id.end()
                                );
        }

        // Payload
        int offset_low = index * step;
        int offset_high = min((index+1)*step, stream_size);
        new_stream.insert( 
                      new_stream.end(),
                      stream.begin() + offset_low,
                      stream.begin() + offset_high
                      );
        bits_written += offset_high - offset_low;
        index++;
    }

    // Sanity checks
    assert(bits_written == stream_size);
    assert(new_stream.size() > stream_size);

    return new_stream;
}

vector<bool> AuroraFormatter::orphan_pad(vector<bool> & stream) {
    vector<bool> new_stream(stream);

    new_stream.reserve( (new_stream.size()%64 + 1) * 64);

    while(new_stream.size()%64 > 0) {
        new_stream.push_back(0);
    }
    return new_stream;
}